import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { IPokemonFull } from 'src/app/models/IPokemonFull';
import { IPokemonResult } from 'src/app/models/IPokemonResult';

@Component({
  selector: 'app-exo5',
  templateUrl: './exo5.component.html',
  styleUrls: ['./exo5.component.scss']
})
export class Exo5Component implements OnInit {
  setUpList : string[] = new Array<string>(20).fill('----------');

  pokemons! : IPokemonResult;
  private _pokemonUrl : string = 'https://pokeapi.co/api/v2/pokemon';

  pokemon! : IPokemonFull;

  constructor(private _httpClient : HttpClient){

  }

  ngOnInit(): void {
      this.getAll();
  }

  getAll() : void {
    this._httpClient.get<IPokemonResult>(this._pokemonUrl).subscribe({
      next : (res) => { this.pokemons = res},
      error : () => {},
      complete : () => {}
    })
  }

  next() : void {
    if(this.pokemons.next != null)
    {
      this._httpClient.get<IPokemonResult>(this.pokemons.next).subscribe({
        next : (res) => { this.pokemons = res},
        error : () => {},
        complete : () => {}
      })
    }
    
  }

  prev() : void {
    if(this.pokemons.previous != null)
    {
      this._httpClient.get<IPokemonResult>(this.pokemons.previous).subscribe({
        next : (res) => { this.pokemons = res},
        error : () => {},
        complete : () => {}
      })
    }
  }

  getOne(url : string) : void {
    this._httpClient.get<IPokemonFull>(url).subscribe({
      next : (res) => {this.pokemon = res}
    })
  }
}
