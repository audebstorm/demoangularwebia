export interface IAuthResult {
    token : string;
    user : IAuthUser
}

export interface IAuthUser {
    id : string;
    firstname : string;
}