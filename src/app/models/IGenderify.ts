export interface IGenderify {
    country_id : string;
    gender : string;
    name : string;
    probability	: number;
}