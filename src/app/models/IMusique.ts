export interface IMusique {
    id : number;
    titre : string;
    artiste : string;
    album : string;
    sortie : number;
    pochette : string;
}