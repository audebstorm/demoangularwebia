import { IPokemonSimple } from "./IPokemonSimple";

export interface IPokemonResult {
    count : number;
    next? : string;
    previous? : string;
    results : IPokemonSimple[]
}