export interface IPokemonFull {
    id : number;
    name : string;
    weight : number;
    height : number;
    sprites : ISprite
}

export interface ISprite {
    front_default : string;
    back_default : string;
}