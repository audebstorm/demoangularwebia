export interface IRegister {
    pseudo : string;
    email : string;
    firstname : string;
    lastname : string;
    password : string;
}