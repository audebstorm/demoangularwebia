import { Component, OnInit } from '@angular/core';
import { IAuthUser } from '../models/IAuthResult';
import { ILink } from '../models/ILink';
import { IUser } from '../models/IUser';
import { AuthService } from '../shared/services/auth.service';
import { FakeAuthService } from '../shared/services/fake-auth.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss'],  
})
export class NavbarComponent implements OnInit {
  links : ILink[] = [
    { title : 'Accueil', url : '/'},
    { title : 'Demo', url : '/demo', children : [
      { title : 'Demo 1 - Les Bindings', url : '/demo/demo1'},
      { title : 'Demo 2 - Les Pipes', url : '/demo/demo2'},
      { title : 'Demo 3 - Les Directives', url : '/demo/demo3'},
      { title : 'Demo 4 - Input & Output', url : '/demo/demo4'},
      { title : 'Demo 5 - Services', url : '/demo/demo5'},
      { title : 'Demo 6 - Les Formulaires', url : '/demo/demo6'},
      { title : 'Demo 7 - Routing -> Fake Crud complet', url : '/demo/demo7'},
      { title : 'Demo 8 - HttpClient -> Les requêtes', url : '/demo/demo8'},
      { title : 'Demo 9 - HttpClient -> Full Crud', url : '/demo/demo9'},
      { title : 'Demo 10 - Demo Auth + Interceptor', url : '/demo/demo10' }
    ], isVisible : false },
    { title : 'Exercice', url : '/exercice', children : [
      { title : 'Exercice 1 - Le timer', url : '/exercice/exo1'},
      { title : 'Exercice 2 - La Shopping List Eco+', url : '/exercice/exo2'},
      { title : 'Exercice 3 - La Shopping List', url : '/exercice/exo3'},
      { title : 'Exercice 4 - Formulaire d\'inscription', url : '/exercice/exo4'},
      { title : 'Exercice 5 - Pokedex', url : '/exercice/exo5'}
    ], isVisible : false}
  ];

  //connectedUser : IUser | undefined;
  connectedUser : IAuthUser | undefined;

  constructor(/*private _authService : FakeAuthService*/ private _authService : AuthService){

  }

  ngOnInit() : void {
    //Avant Observable
    //this.connectedUser = this._authService.connectedUser;

    //Avec Observable
    // this._authService.$connectedUser.subscribe({
    //   next : (user : IUser | undefined) => {
    //     this.connectedUser = user;
    //     console.log("Navbar change user", user);
        
    //   },
    //   error : () => {},
    //   complete : () => {}
    // })

    //Avec vraie Auth
    this._authService.$connectedUser.subscribe({
      next : (user : IAuthUser | undefined) => {
        this.connectedUser = user;
        console.log("Navbar change user", user);
        
      },
      error : () => {},
      complete : () => {}
    })
  }


  toggleLink(i : number) : void {
    for(let j : number = 0; j < this.links.length; j++ ) {
      if(j != i) {
        this.links[j].isVisible = false;
      }
    }
    this.links[i].isVisible = !this.links[i].isVisible;

    //autre façon
    // let temp : boolean | undefined = this.links[i].isVisible;
    // this.links.forEach((link : ILink) => link.isVisible = false)
    // this.links[i].isVisible = !temp;

  }

  disconnect() : void {
    this._authService.logout();
  }
}
