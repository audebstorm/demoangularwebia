import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, Subject } from 'rxjs';
import { IUser } from 'src/app/models/IUser';

@Injectable({
  providedIn: 'root'
})
export class FakeAuthService {

  private _users: IUser[] = [
    { id: 1, pseudo: 'audeb', password: '123aude', firstname: 'Aude', lastname: 'Beurive' },
    { id: 2, pseudo: 'khunl', password: '123khun', firstname: 'Khun', lastname: 'Ly' }
  ];

  //avant Observable
  //connectedUser : IUser | undefined;

  //avec Observable
  //Le Subject se déclenchera au premier .next (évènement)
  private _$connectedUser: Subject<IUser | undefined> = new Subject<IUser | undefined>();
  
  //Le Behavior se déclenchera au subscribe desuss + chaque évènement (next)
  //En gros il est déjà initialisé avec une valeur (ici, khun)
  //private _$connectedUser : BehaviorSubject<IUser | undefined> = new BehaviorSubject<IUser | undefined>(this._users[1]);

  //Pour empêcher qu'un composant puisse déclencher le subject avec .next (ou error etc), on le transforme en Observable, et c'est lui qu'on rend disponible en public :
  //On ne peut que s'abonner sur un object de type Observable
  $connectedUser : Observable<IUser | undefined> = this._$connectedUser.asObservable()

  constructor() { }

  connection(pseudo: string, pwd: string): /*IUser | undefined*/ void {
    //Est-ce que j'ai un user dont le pseudo correspond à celui que j'reçois ?
    let userFound = this._users.find(u => u.pseudo === pseudo);
    //Si oui
    if (userFound) {     
      //Est-ce que le mdp correspond à celui de l'utilisateur trouvé ?
      if (userFound.password === pwd) {       
        //Si mdp égaux, on renvoie l'utilisateur trouvé
        //Si on veut stocker en sessionStorage : S'efface dès qu'on ferme le navigateur ou l'onglet
        //sessionStorage.setItem('userFirstname', userFound.firstname);
        //Stockage du prénom dans le localStorage : S'efface si l'utilisateur vide le cache ou si l'application vide le localStorage
        localStorage.setItem('userFirstname', userFound.firstname);
        //Pour récupérer le prénom
        //let prenom : string | null = localStorage.getItem('userFirstname');

        //avant Observable
        //this.connectedUser = userFound;

        //avec Observable
        this._$connectedUser.next(userFound);
        //return userFound;
      }
      else {
        this._$connectedUser.next(undefined);
        //this.$connectedUser.error('Password incorrect');

      }
    }
    else {
      //avant Observable
      //this.connectedUser = undefined;

      //avec Observable 
      this._$connectedUser.next(undefined);
      //return undefined;
    }


  }

  disconnection(): /*undefined*/ void {
    //En vrai, il faudra, quand on aura vu une vraie Auth, dégager le token de la session etc
    //On enlève le prénom du localStorage
    //localStorage.clear() //attention vide TOUT votre localStorage
    localStorage.removeItem('userFirstname'); //Enlève l'élément userFirstname stocké

    //avant observable
    //this.connectedUser = undefined;

    //avec Observable
    this._$connectedUser.next(undefined);
    //return undefined;
  }

}
