import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { IUser2 } from 'src/app/models/IUser2';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  private _url : string = 'http://localhost:8080/api/user';


  constructor(private _httpClient : HttpClient) { }



  getAll() : Observable<IUser2[]> { 
    //let token : string = localStorage.getItem('token')?? '';
    // return this._httpClient.get<IUser2[]>(this._url, { headers : { 'Authorization' : 'Bearer ' + token  }});
    return this._httpClient.get<IUser2[]>(this._url);
  }
}
