import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { IAuthResult, IAuthUser } from 'src/app/models/IAuthResult';
import { ILogin } from 'src/app/models/ILogin';
import { IRegister } from 'src/app/models/IRegister';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private _url : string = 'http://localhost:8080/api/auth/';

  private _$connectedUser : Subject<IAuthUser | undefined> = new Subject<IAuthUser | undefined>();

  $connectedUser : Observable<IAuthUser | undefined> = this._$connectedUser.asObservable();

  constructor(private _httpClient : HttpClient) { }

  getConnectedUser() : void {
    //On récup l'id dans le storage (ou pas)
    let userId : string | null = localStorage.getItem('userId');
    //Si on a un id, on fait une requête pour récup le user correspondant
    if(userId) {
      this._httpClient.get<IAuthUser>(`http://localhost:8080/api/user/${userId}`).subscribe({
          next : (res) => { 
            this._$connectedUser.next(res);
          },
          error : () => {
            this._$connectedUser.next(undefined);
          }    
      })
    }
    else {
      this._$connectedUser.next(undefined);
    }
  }

  register(register : IRegister) : void {
    this._httpClient.post<IAuthResult>(this._url+'register', register).subscribe({
      next : (res) => {
        //On emet le user qui vient de s'enregistrer via l'observable connectedUser
        this._$connectedUser.next(res.user);
        //Mettre le token et le user en LocalStorage
        localStorage.setItem('token', res.token);
        localStorage.setItem('userId', res.user.id);
      }
    })
  }

  login(loginForm : ILogin) : void {
    this._httpClient.post<IAuthResult>(this._url+'login', loginForm).subscribe({
      next : (res) => {
        //On emet le user qui vient de s'enregistrer via l'observable connectedUser
        this._$connectedUser.next(res.user);
        //Mettre le token et le user en LocalStorage
        localStorage.setItem('token', res.token);
        localStorage.setItem('userId', res.user.id);
      }
    })
  }

  logout() : void {
    this._$connectedUser.next(undefined);
    localStorage.removeItem('token');
    localStorage.removeItem('userId');
  }
}
