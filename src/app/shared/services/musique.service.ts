import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { IMusique } from 'src/app/models/IMusique';

@Injectable({
  providedIn: 'root'
})
export class MusiqueService {

  private _url : string = 'http://localhost:3000/musique';

  constructor(private _httpClient : HttpClient) { }

  getAll() : Observable<IMusique[]> {
    return this._httpClient.get<IMusique[]>(this._url);
  }

  getById(id : number) : Observable<IMusique> {
    return this._httpClient.get<IMusique>(this._url+'/'+id);
  }

  create(musique : IMusique) : Observable<IMusique> {
    return this._httpClient.post<IMusique>(this._url, musique);
  }

  update(id : number, musique : IMusique) : Observable<IMusique> {
    return this._httpClient.put<IMusique>(this._url+'/'+id, musique);
  }

  delete(id : number) : Observable<IMusique> {
    return this._httpClient.delete<IMusique>(this._url+'/'+id);
  }
}
