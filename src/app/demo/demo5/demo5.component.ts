import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { IUser } from 'src/app/models/IUser';
import { FakeAuthService } from 'src/app/shared/services/fake-auth.service';

@Component({
  selector: 'app-demo5',
  templateUrl: './demo5.component.html',
  styleUrls: ['./demo5.component.scss']
})
export class Demo5Component implements OnInit, OnDestroy {

  connectedUser : IUser | undefined;
  pseudo : string = '';
  mdp : string = '';

  errorMessage : string = '';

  monSub! : Subscription;

  constructor(private _userService : FakeAuthService){

  }

  ngOnInit(): void {
      this.monSub = this._userService.$connectedUser.subscribe({
        //Ce qu'on doit faire pour chaque changement de valeur de l'observable
        next : (user : IUser | undefined) => {
          this.connectedUser = user;
          console.log("Demo5 changement user", user);
          
        },
        //Ce qu'on doit faire en cas d'erreur
        error : (err) => {
          console.log(err);
          this.errorMessage = err;          
        },
        //Ce qu'on doit faire quand l'Observable meurt
        complete : () => {}
      })

      //sera d'office le next
      //this._userService.$connectedUser.subscribe(() => {})
  }

  //Méthode appelée quand le composant n'est pas affiché à l'écran
  ngOnDestroy(): void {
      console.log('Destroy');
      //Comme le composant n'est pas là, on a aucune raison de resté abonné, on va donc se désabonner    
      //De plus, si on ne se désabonne pas, on va s'abonner autant de fois qu'on va init le component et chaque next va déclencher autant d'abo 
      this.monSub.unsubscribe();
  }

  connect() : void {
    //avant Observable
    //this.connectedUser = this._userService.connection(this.pseudo, this.mdp);

    //avec Observable
    this._userService.connection(this.pseudo, this.mdp);

    this.pseudo = '';
    this.mdp = '';
  }

  disconnect() : void {
    //avant Observable
    //this.connectedUser = this._userService.disconnection();

    //avec Observable
    this._userService.disconnection();
  }
}
