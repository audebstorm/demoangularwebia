import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DemoRoutingModule } from './demo-routing.module';
import { DemoComponent } from './demo.component';
import { Demo1Component } from './demo1/demo1.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Demo2Component } from './demo2/demo2.component';
import { SharedModule } from '../shared/shared.module';
import { Demo3Component } from './demo3/demo3.component';
import { Demo4Component } from './demo4/demo4.component';
import { EnfantComponent } from './demo4/enfant/enfant.component';
import { Demo5Component } from './demo5/demo5.component';
import { Demo6Component } from './demo6/demo6.component';
import { Demo7Component } from './demo7/demo7.component';
import { Demo7CreateComponent } from './demo7/demo7-create/demo7-create.component';
import { Demo7EditComponent } from './demo7/demo7-edit/demo7-edit.component';
import { Demo8Component } from './demo8/demo8.component';
import { HttpClientModule } from '@angular/common/http';
import { Demo9Component } from './demo9/demo9.component';
import { CreateMusicComponent } from './demo9/create-music/create-music.component';
import { EditMusicComponent } from './demo9/edit-music/edit-music.component';
import { Demo10Component } from './demo10/demo10.component';
import { RegisterComponent } from './demo10/register/register.component';
import { LoginComponent } from './demo10/login/login.component';

@NgModule({
  declarations: [
    DemoComponent,
    Demo1Component,
    Demo2Component,
    Demo3Component,
    Demo4Component,
    EnfantComponent,
    Demo5Component,
    Demo6Component,
    Demo7Component,
    Demo7CreateComponent,
    Demo7EditComponent,
    Demo8Component,
    Demo9Component,
    CreateMusicComponent,
    EditMusicComponent,
    Demo10Component,
    RegisterComponent,
    LoginComponent,
  ],
  imports: [
    CommonModule,
    DemoRoutingModule,
    FormsModule, //Pour utiliser ngModel (le binding two way)
    SharedModule,
    ReactiveFormsModule, //Pour utiliser tout ce qui est relatif au ReactiveForms (formGroup, formControl etc)
    //HttpClientModule
  ],
  bootstrap : [DemoComponent]

})
export class DemoModule { }
