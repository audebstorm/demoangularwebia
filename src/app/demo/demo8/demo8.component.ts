import { HttpClient } from '@angular/common/http';
import { Component } from '@angular/core';
import { IGenderify } from 'src/app/models/IGenderify';

@Component({
  selector: 'app-demo8',
  templateUrl: './demo8.component.html',
  styleUrls: ['./demo8.component.scss']
})
export class Demo8Component {
  genderify!: IGenderify;

  prenom: string = '';
  codePays: string = '';

  constructor(private _httpClient: HttpClient) {
    
  }

  lancerRecherche() {
    console.log(this.prenom);
    console.log(this.codePays);
    if (this.prenom !== '' && this.codePays !== '') {
      this._httpClient.get<IGenderify>(`https://api.genderize.io/?name=${this.prenom}&country_id=${this.codePays}`).subscribe({
        next: (res) => { 
          this.genderify = res;
          console.log(this.genderify);
          
        },
        error: () => { },
        complete: () => { }
      });

    }
  }
}
