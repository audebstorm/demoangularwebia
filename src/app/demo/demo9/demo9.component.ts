import { Component, OnInit } from '@angular/core';
import { IMusique } from 'src/app/models/IMusique';
import { MusiqueService } from 'src/app/shared/services/musique.service';

@Component({
  selector: 'app-demo9',
  templateUrl: './demo9.component.html',
  styleUrls: ['./demo9.component.scss']
})
export class Demo9Component implements OnInit {

  listMusic : IMusique[] = [];

  constructor(private _musiqueService : MusiqueService) {

  }

  ngOnInit(): void {
      this._musiqueService.getAll().subscribe({
        next : (res) => { this.listMusic = res }
      })
  }

  delete(id : number) : void {
    this._musiqueService.delete(id).subscribe({
      next : () => {
        //La db ayant été modifiée lors de la suppression si tout s'est bien passé, on met la liste des musiques à jour
        this._musiqueService.getAll().subscribe({
          next : (res) => { this.listMusic = res }
        })
      }
    })
  }
}
