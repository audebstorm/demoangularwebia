import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { IMusique } from 'src/app/models/IMusique';
import { MusiqueService } from 'src/app/shared/services/musique.service';

@Component({
  selector: 'app-create-music',
  templateUrl: './create-music.component.html',
  styleUrls: ['./create-music.component.scss']
})
export class CreateMusicComponent {

  ajoutForm : FormGroup;

  constructor(private _fb : FormBuilder, private _musiqueService : MusiqueService, private _route : Router) {
    this.ajoutForm = this._fb.group({
      titre : [null, [Validators.required]],
      artiste : [null, [Validators.required]],
      album : [null, [Validators.required]],
      sortie : [new Date().getFullYear, [Validators.required]],
      pochette : [null, [Validators.required]]
    })

  }

  add() : void {
    if(this.ajoutForm.valid)
    {
      let musique : IMusique = this.ajoutForm.value;
      musique.id = 0;
      this._musiqueService.create(musique).subscribe({
        next : () => {
          this._route.navigateByUrl('/demo/demo9');
        }
      })
    }
  }
}
