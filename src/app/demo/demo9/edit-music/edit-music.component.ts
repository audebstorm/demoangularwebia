import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { IMusique } from 'src/app/models/IMusique';
import { MusiqueService } from 'src/app/shared/services/musique.service';

@Component({
  selector: 'app-edit-music',
  templateUrl: './edit-music.component.html',
  styleUrls: ['./edit-music.component.scss']
})
export class EditMusicComponent implements OnInit {
  editForm : FormGroup;
  id : number;

  constructor(private _fb : FormBuilder, private _musiqueService : MusiqueService, private _route : Router, private _activeRoute : ActivatedRoute) {

    this.id = parseInt(this._activeRoute.snapshot.params['id']);

    this.editForm = this._fb.group({
      titre : [null, [Validators.required]],
      artiste : [null, [Validators.required]],
      album : [null, [Validators.required]],
      sortie : [new Date().getFullYear, [Validators.required]],
      pochette : [null, [Validators.required]]
    })

  }

  ngOnInit(): void {
      this._musiqueService.getById(this.id).subscribe({
        next : (res) => {
          this.editForm.patchValue({
            titre : res.titre,
            artiste : res.artiste,
            album : res.album,
            sortie : res.sortie,
            pochette : res.pochette
          })
        }
      })
  }

  edit() : void {
    if(this.editForm.valid)
    {
      let musique : IMusique = this.editForm.value;
      musique.id = this.id;
      this._musiqueService.update(this.id,musique).subscribe({
        next : () => {
          this._route.navigateByUrl('/demo/demo9');
        }
      })
    }
  }
}
