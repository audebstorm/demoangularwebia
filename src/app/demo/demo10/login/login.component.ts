import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ILogin } from 'src/app/models/ILogin';
import { AuthService } from 'src/app/shared/services/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  loginForm : FormGroup;

  constructor(private _fb : FormBuilder, private _authService : AuthService, private _route : Router) {
    this.loginForm = this._fb.group({
      credential : [null, [Validators.required]],
      password : [null, [Validators.required]]
    })
  }

  ngOnInit(): void {
      this._authService.$connectedUser.subscribe({
        next : (res) => {
          if(res){
            this._route.navigateByUrl('/')
          }
        }
      })
  }

  login() : void {
    console.log(this.loginForm.value);
    
    if(this.loginForm.valid){
      let loginBody : ILogin = this.loginForm.value;
      this._authService.login(loginBody);
    }

  }
}
