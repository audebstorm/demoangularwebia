import { Component, OnInit } from '@angular/core';
import { IUser2 } from 'src/app/models/IUser2';
import { UserService } from 'src/app/shared/services/user.service';

@Component({
  selector: 'app-demo10',
  templateUrl: './demo10.component.html',
  styleUrls: ['./demo10.component.scss']
})
export class Demo10Component implements OnInit {
  users : IUser2[] = [];
  unauthorized : boolean = false;

  constructor(private _userService : UserService) {

  }

  ngOnInit(): void {
      this._userService.getAll().subscribe({
        next : (res) => {
          this.users = res;
          this.unauthorized = false;
        },
        error : (err) => {
          if(err.status == 401){
            this.unauthorized = true;
          }
          console.log('Erreur ‼', err);
          
        }
      })
  }
}
