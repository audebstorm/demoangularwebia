import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ConnectedGuard } from '../shared/guards/connected.guard';
import { Demo1Component } from './demo1/demo1.component';
import { Demo10Component } from './demo10/demo10.component';
import { LoginComponent } from './demo10/login/login.component';
import { RegisterComponent } from './demo10/register/register.component';
import { Demo2Component } from './demo2/demo2.component';
import { Demo3Component } from './demo3/demo3.component';
import { Demo4Component } from './demo4/demo4.component';
import { Demo5Component } from './demo5/demo5.component';
import { Demo6Component } from './demo6/demo6.component';
import { Demo7CreateComponent } from './demo7/demo7-create/demo7-create.component';
import { Demo7EditComponent } from './demo7/demo7-edit/demo7-edit.component';
import { Demo7Component } from './demo7/demo7.component';
import { Demo8Component } from './demo8/demo8.component';
import { CreateMusicComponent } from './demo9/create-music/create-music.component';
import { Demo9Component } from './demo9/demo9.component';
import { EditMusicComponent } from './demo9/edit-music/edit-music.component';

const routes: Routes = [
  { path : 'demo1', component : Demo1Component },
  { path : 'demo2', component : Demo2Component, canActivate : [ConnectedGuard]},
  { path : 'demo3', component : Demo3Component},
  { path : 'demo4', component : Demo4Component},
  { path : 'demo5', component : Demo5Component},
  { path : 'demo6', component : Demo6Component},
  { path : 'demo7', component : Demo7Component},
  { path : 'ajoutFormateur', component : Demo7CreateComponent},
  { path : 'editFormateur/:id', component : Demo7EditComponent},
  { path : 'demo8', component : Demo8Component},
  { path : 'demo9', component : Demo9Component},
  { path : 'ajoutMusique', component : CreateMusicComponent},
  { path : 'editMusique/:id', component : EditMusicComponent},
  { path : 'demo10', component : Demo10Component},
  { path : 'login', component : LoginComponent},
  { path : 'register', component : RegisterComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DemoRoutingModule { }
